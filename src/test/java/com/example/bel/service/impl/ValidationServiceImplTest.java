package com.example.bel.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ValidationServiceImplTest {
	
	@InjectMocks
	private ValidationServiceImpl validationService;
	
	@Test(expected = Exception.class)
	public void settingTest() throws Exception {
		this.validationService.validateEntryValues("message", false, false, false, false, false, false);
	}
	
	@Test(expected = Exception.class)
	public void logTypeTest() throws Exception {
		this.validationService.validateEntryValues("message", true, false, false, false, false, false);
	}
	
	@Test(expected = Exception.class)
	public void messageNullTest() throws Exception {
		this.validationService.validateEntryValues(null, true, false, false, false, true, false);
	}

}
