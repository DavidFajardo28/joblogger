package com.example.bel.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.bel.util.Constants;

@RunWith(MockitoJUnitRunner.class)
public class BuildLogServiceImplTest {
	
	@InjectMocks
	private BuildLogServiceImpl buildLogService;
	
	@Test
	public void buildGenericLogLogMessageTest() {
		final String messageText = "Test message";
		final boolean logError = false;
		final boolean logWarning = false;
		final boolean logMessage = true;
		final String message = this.buildLogService.buildGenericLog(messageText, logError, logWarning, logMessage);
		assertTrue(message.contains(Constants.MESSAGE));
	}
	
	@Test
	public void buildGenericLogLogWarningTest() {
		final String messageText = "Test message";
		final boolean logError = false;
		final boolean logWarning = true;
		final boolean logMessage = false;
		final String message = this.buildLogService.buildGenericLog(messageText, logError, logWarning, logMessage);
		assertTrue(message.contains(Constants.WARNING));
	}
	
	@Test
	public void buildGenericLogLogErrorTest() {
		final String messageText = "Test message";
		final boolean logError = true;
		final boolean logWarning = false;
		final boolean logMessage = false;
		final String message = this.buildLogService.buildGenericLog(messageText, logError, logWarning, logMessage);
		assertTrue(message.contains(Constants.ERROR));
	}
	
	@Test
	public void buildLogToDataBaseLogMessageTest() {
		final boolean logError = false;
		final boolean logWarning = false;
		final boolean logMessage = true;
		final int messageType = this.buildLogService.buildLogToDataBase(logError, logWarning, logMessage);
		assertEquals(1, messageType);
	}
	
	@Test
	public void buildLogToDataBaseLogWarningTest() {
		final boolean logError = false;
		final boolean logWarning = true;
		final boolean logMessage = false;
		final int messageType = this.buildLogService.buildLogToDataBase(logError, logWarning, logMessage);
		assertEquals(3, messageType);
	}
	
	@Test
	public void buildLogToDataBaseLogErrorTest() {
		final boolean logError = true;
		final boolean logWarning = false;
		final boolean logMessage = false;
		final int messageType = this.buildLogService.buildLogToDataBase(logError, logWarning, logMessage);
		assertEquals(2, messageType);
	}

}
