package com.example.bel;

import java.util.Map;
import java.util.logging.Logger;

import com.example.bel.service.BuildLogService;
import com.example.bel.service.LogOutputService;
import com.example.bel.service.ValidationService;

public class CustomJobLogger {
	
	private static boolean logToConsole;
	private static boolean logToFile;
	private static boolean logToDatabase;
	
	private static boolean logMessage;
	private static boolean logWarning;
	private static boolean logError;
	
	private static Map<String, String> dbParams;
	private static Logger logger;
	
	private BuildLogService buildLogService;
	private LogOutputService logOutputService;
	private ValidationService validationService;
	
	public CustomJobLogger(boolean logToConsole, boolean logToFile, boolean logToDatabase, boolean logMessage,
			boolean logWarning, boolean logError, Map<String, String> dbParams, BuildLogService buildLogService,
			LogOutputService logOutputService, ValidationService validationService) {
		
		this.logToConsole = logToConsole;
		this.logToFile = logToFile;
		this.logToDatabase = logToDatabase;
		
		this.logMessage = logMessage;
		this.logWarning = logWarning;
		this.logError = logError;
		
		this.dbParams = dbParams;
		this.logger = Logger.getLogger("MyLog");
		
		this.buildLogService = buildLogService;
		this.logOutputService = logOutputService;
		this.validationService = validationService;
		
	}
	
	public void runCustomJobLogger(final String messageText) throws Exception {
		
		this.validationService.validateEntryValues(messageText, logToConsole, logToFile, logToDatabase,
				logError, logMessage, logWarning);
		
		final String message = this.buildLogService.buildGenericLog(messageText, logError, logWarning, logMessage);
		
		if (logToConsole) {
			this.logOutputService.logOutputOnConsole(message, logger);
		}
		
		if (logToFile) {
			this.logOutputService.logOutputOnFile(message, dbParams, logger);
		}
		
		if (logToDatabase) {
			final int messageType = this.buildLogService.buildLogToDataBase(logError, logWarning, logMessage);
			this.logOutputService.logOutputOnDataBase(messageText, messageType, dbParams);
		}
		
	}
	
	
	/**
	 * tests executed at each log output [console, file, db]
	public static void main (String[] args) {
		
		Map<String, String> params = new HashMap<>();
		params.put(Constants.FOLDER, "src");
		params.put(Constants.DB_USERNAME, "root");
		params.put(Constants.DB_PASSWORD, "toor");
		params.put(Constants.DBMS, "mysql");
		params.put(Constants.SERVER_NAME, "localhost");
		params.put(Constants.PORT_NUMBER, "3306");
		params.put(Constants.DB_NAME, "test?serverTimezone=America/Lima");
		
		CustomJobLogger customJobLogger = new CustomJobLogger(false, false, true, true, false, false, params,
				new BuildLogServiceImpl(), new LogOutputServiceImpl(new DataBaseConnectionServiceImpl()), new ValidationServiceImpl());
		
		try {
			customJobLogger.runCustomJobLogger("Test");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

}
