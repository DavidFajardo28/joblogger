package com.example.bel.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.bel.service.DataBaseConnectionService;
import com.example.bel.util.Constants;
import com.example.bel.util.Util;

public class DataBaseConnectionServiceImpl implements DataBaseConnectionService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DataBaseConnectionServiceImpl.class);

	@Override
	public Statement setConnection(Map<String, String> dbParams) {
		Connection connection = null;
		Statement statement = null;
		try {
			connection = DriverManager.getConnection(
					Util.getUrl(dbParams.get(Constants.DBMS), dbParams.get(Constants.SERVER_NAME),
							dbParams.get(Constants.PORT_NUMBER), dbParams.get(Constants.DB_NAME)),
					dbParams.get(Constants.DB_USERNAME), dbParams.get(Constants.DB_PASSWORD));
			
			statement = connection.createStatement();
		} catch (SQLException e) {
			LOGGER.error("Error la establecer la conexión: {}", e.getMessage());
		} finally {
			/**if (connection != null) {
				try {*/
					LOGGER.info("Conexion abierta");
					/**connection.close();
				} catch (SQLException e) {
					LOGGER.error("Error la cerrar la conexión: {}", e.getMessage());
				}
			}*/
		}
		return statement;
	}

}
