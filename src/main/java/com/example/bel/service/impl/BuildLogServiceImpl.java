package com.example.bel.service.impl;

import java.text.DateFormat;
import java.util.Date;

import com.example.bel.service.BuildLogService;
import com.example.bel.util.Constants;

public class BuildLogServiceImpl implements BuildLogService {

	@Override
	public String buildGenericLog(final String messageText, final boolean logError,
			final boolean logWarning, final boolean logMessage) {
		
		StringBuilder message = new StringBuilder(Constants.EMPTY);
		if (logError) {
			message.append(Constants.ERROR);
		}

		if (logWarning) {
			message.append(Constants.WARNING);
		}

		if (logMessage) {
			message.append(Constants.MESSAGE);
		}
		
		message.append(DateFormat.getDateInstance(DateFormat.LONG).format(new Date())).append(messageText);
		
		return message.toString();
	}

	@Override
	public int buildLogToDataBase(boolean logError, boolean logWarning, boolean logMessage) {
		
		int t = 0;
		if (logMessage) {
			t = 1;
		}

		if (logError) {
			t = 2;
		}

		if (logWarning) {
			t = 3;
		}
		
		return t;
	}

}
