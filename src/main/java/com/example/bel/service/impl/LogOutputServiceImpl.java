package com.example.bel.service.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.bel.service.DataBaseConnectionService;
import com.example.bel.service.LogOutputService;
import com.example.bel.util.Constants;

import lombok.AllArgsConstructor;

@AllArgsConstructor(onConstructor_ = {@Autowired})
public class LogOutputServiceImpl implements LogOutputService {
	
	private DataBaseConnectionService dataBaseConnectionService;

	@Override
	public void logOutputOnConsole(String message, Logger logger) {
		ConsoleHandler ch = new ConsoleHandler();
	    logger.addHandler(ch);
	    logger.log(Level.INFO, message);
	}
	
	@Override
	public void logOutputOnFile(String message, Map<String, String> dbParams, Logger logger) throws IOException {
		FileHandler fileHandler = new FileHandler(dbParams.get(Constants.FOLDER) + Constants.SLASH + Constants.TEXT_FILE);
	    logger.addHandler(fileHandler);
	    logger.log(Level.INFO, message);
	}

	@Override
	public void logOutputOnDataBase(String messageText, int messageType, Map<String, String> dbParams) throws SQLException {
		Statement stmt = this.dataBaseConnectionService.setConnection(dbParams);
	    stmt.executeUpdate("insert into log (message, type) values ('" + messageText + "', " + messageType + ")");
	}

}
