package com.example.bel.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Logger;

public interface LogOutputService {

	void logOutputOnConsole(String message, Logger logger);
	
	void logOutputOnFile(String message, Map<String, String> dbParams, Logger logger) throws IOException;

	void logOutputOnDataBase(String messageText, int messageType, Map<String, String> dbParams) throws SQLException;

}
