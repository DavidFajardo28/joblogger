package com.example.bel.service;

import java.sql.Statement;
import java.util.Map;

public interface DataBaseConnectionService {
	
	Statement setConnection(Map<String, String> dbParams);

}
