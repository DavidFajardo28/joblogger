package com.example.bel.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {
	
	public static final String DB_USERNAME = "username";
	public static final String DB_PASSWORD = "password";
	public static final String JDBC = "jdbc:";
	public static final String TSS = "://";
	public static final String TWO_POINTS = ":";
	public static final String SLASH = "/";
	public static final String DBMS = "dbms";
	public static final String SERVER_NAME = "server_name";
	public static final String PORT_NUMBER = "port_number";
	public static final String DB_NAME = "db_name";
	public static final String ERROR = "error";
	public static final String WARNING = "warning";
	public static final String MESSAGE = "message";
	public static final String EMPTY = "";
	
	public static final String FOLDER = "logFileFolder";
	public static final String TEXT_FILE = "logFile.txt";

}
