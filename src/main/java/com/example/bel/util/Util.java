package com.example.bel.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Util {
	
	public static String getUrl(final String dbms, final String serverName, final String portNumer,
			final String dataBaseName) {
		StringBuilder url = new StringBuilder(Constants.JDBC);
		url.append(dbms).append(Constants.TSS).append(serverName).append(Constants.TWO_POINTS)
		.append(portNumer).append(Constants.SLASH).append(dataBaseName);
		return url.toString();
	}

}
