package com.example.bel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelApplication.class, args);
	}

}
